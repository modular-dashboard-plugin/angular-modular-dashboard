import {UploadResponse} from "./upload-response";
import {Observable} from "rxjs";

export interface BaseFileServer {
  push(file: File): UploadResponse

  delete(path: string): Observable<void>

  getName(path: string): Observable<string>
}
