export class ApiError {
  constructor(public statusCode: number, public errorCode: string, public errorMessage: string = 'Something went wrong.') {

  }
}
