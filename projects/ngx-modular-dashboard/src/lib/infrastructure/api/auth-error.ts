import {ApiError} from "./api-error";

export class AuthError extends ApiError {
  constructor(public errorCode: string, public errorMessage: string = 'Something went wrong.') {
    super(401, errorCode, errorMessage);
  }
}
