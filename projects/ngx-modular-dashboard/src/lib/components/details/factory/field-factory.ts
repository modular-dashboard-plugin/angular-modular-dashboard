import {FieldStructure, FieldViewStructure} from "../model/field-structure";
import {FlexFactory} from "../../common/flex-factory";
import {FieldType} from "../model/field-type";


export class FieldFactory {

  static field(structure: FieldStructure): FieldViewStructure {
    return {
      id: structure.id,
      title: structure.title,
      transform: structure.transform,
      dimensions: structure.dimensions !== undefined ? structure.dimensions : FlexFactory.defaultFlex(),
      type: FieldType.VALUE
    }
  }

  static text(structure: FieldStructure): FieldViewStructure {
    return {
      id: structure.id,
      title: structure.title,
      transform: structure.transform,
      dimensions: structure.dimensions !== undefined ? structure.dimensions : FlexFactory.defaultFlex(),
      type: FieldType.TEXT
    }
  }

  static title(title: string): FieldViewStructure {
    return {
      id: '',
      title: title,
      transform: undefined,
      dimensions: FlexFactory.fixedFlex(100),
      type: FieldType.TITLE
    }
  }

}
