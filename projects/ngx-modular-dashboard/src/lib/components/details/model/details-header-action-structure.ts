export interface DetailsHeaderActionViewStructure {
  readonly name: string
  readonly icon: string | null
  readonly display: () => boolean
  readonly callback: (data: any) => void
}

export interface DetailsHeaderActionStructure {
  readonly name: string
  readonly icon?: string
  readonly display?: () => boolean
  readonly callback: (data: any) => void
}
