import {Dimensions} from "../../common/dimensions";
import {FieldType} from "./field-type";

export interface FieldStructure {
  id: string
  title: string
  transform?: (data: any) => string
  dimensions?: Dimensions
}



export interface FieldViewStructure {
  id: string
  title: string
  transform?: (data: any) => string
  dimensions: Dimensions,
  type : FieldType
}
