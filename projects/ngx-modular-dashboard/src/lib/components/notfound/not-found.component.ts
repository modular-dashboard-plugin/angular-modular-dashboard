import {Component, EventEmitter, Input, Output} from '@angular/core'


@Component({
  selector: 'ng-modular-not-found',
  template: `
    <div class="content">
      <div class="content-panel" fxLayout="column" fxLayoutAlign="start center">
        <div class="main">
          <img [src]="notFoundImage" alt="Not Found">
        </div>
        <div>
          <button color="warn" mat-stroked-button (click)="goHome.next()">Go Home</button>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

  @Input() notFoundImage! :string
  @Output() goHome :EventEmitter<void> = new EventEmitter<void>()

}
