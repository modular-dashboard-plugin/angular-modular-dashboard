// noinspection JSUnusedGlobalSymbols

import {
  ColumnActionStructure,
  ColumnAsyncSelectFilterStructure,
  ColumnNoFilterStructure,
  ColumnSelectFilterStructure,
  ColumnTypeFilterStructure
} from "../model/column-structure";
import {TableColumnStructure} from "../model/table-column-structure";
import {FilterType} from "../model/enums/filter-type";
import {ColumnType} from "../model/enums/column-type";
import {FilterStructure} from "../model/filter-structure";
import {Operator} from "../model/enums/operator";

export class ColumnFactory {

  static filterColumn(structure: ColumnTypeFilterStructure): TableColumnStructure {

    let filterId = structure.filterId != undefined ? structure.filterId : structure.columnId

    let filter = {
      filterId: structure.type !== FilterType.DATE_RANG ? `${filterId}_filter` : `${filterId}_filter_start`,
      operatorId: structure.type !== FilterType.DATE_RANG ? `${filterId}_filter_operator` : `${filterId}_filter_end`,
      placeHolder: structure.label,
      type: structure.type,
      operators: ColumnFactory.resolveOperator(structure.type),
      list: []
    }

    return {
      id: structure.columnId,
      filter: filter,
      active: structure.hide != undefined ? !structure.hide : true,
      width: ColumnFactory.resolveWidth(structure.type),
      displayWidth: ColumnFactory.resolveWidth(structure.type),
      name: structure.label,
      center: structure.center != undefined ? structure.center : true,
      sortable: structure.sortable != undefined ? structure.sortable : true,
      type: ColumnType.PLAIN,
      customContent: structure.customContent,
      customContentSelector: structure.customContentSelector,
      actions: [],
      onClick: structure.click
    }
  }

  static selectFilterColumn(structure: ColumnSelectFilterStructure): TableColumnStructure {

    let filterId = structure.filterId != undefined ? structure.filterId : structure.columnId

    let filter: FilterStructure = {
      filterId: `${filterId}_filter`,
      operatorId: `${filterId}_filter_operator`,
      placeHolder: structure.label,
      type: FilterType.SELECT,
      operators: ColumnFactory.resolveOperator(FilterType.SELECT),
      list: structure.list
    }

    if (structure.noFilter) {
      filter = ColumnFactory.noFilter(structure.columnId)
    }

    return this.selectColumn(structure, filter)
  }

  static asyncSelectFilterColumn(structure: ColumnAsyncSelectFilterStructure): TableColumnStructure {

    let filterId = structure.filterId != undefined ? structure.filterId : structure.columnId

    let filter: FilterStructure = {
      filterId: `${filterId}_filter`,
      operatorId: `${filterId}_filter_operator`,
      placeHolder: structure.label,
      type: FilterType.ASYNC_SELECT,
      operators: ColumnFactory.resolveOperator(FilterType.ASYNC_SELECT),
      asyncList: structure.list
    }

    if (structure.noFilter) {
      filter = ColumnFactory.noFilter(structure.columnId)
    }

    return this.selectColumn(structure, filter)
  }


  private static selectColumn(structure: ColumnSelectFilterStructure | ColumnAsyncSelectFilterStructure, filter: FilterStructure): TableColumnStructure {
    return {
      id: structure.columnId,
      filter: filter,
      active: structure.hide != undefined ? !structure.hide : true,
      width: ColumnFactory.resolveWidth(FilterType.SELECT),
      displayWidth: ColumnFactory.resolveWidth(FilterType.SELECT),
      name: structure.label,
      center: structure.center != undefined ? structure.center : true,
      sortable: structure.sortable != undefined ? structure.sortable : true,
      type: ColumnType.SELECT,
      customContent: structure.customContent,
      customContentSelector: structure.customContentSelector,
      actions: []
    };
  }

  static noFilterColumn(structure: ColumnNoFilterStructure): TableColumnStructure {
    return {
      id: structure.columnId,
      filter: ColumnFactory.noFilter(structure.columnId),
      active: structure.hide != undefined ? !structure.hide : true,
      width: 100,
      displayWidth: 100,
      name: structure.label,
      center: structure.center != undefined ? structure.center : true,
      sortable: structure.sortable != undefined ? structure.sortable : true,
      type: ColumnType.PLAIN,
      customContent: structure.customContent,
      customContentSelector: structure.customContentSelector,
      actions: [],
      onClick: structure.click
    }
  }

  static actionColumn(structure: ColumnActionStructure): TableColumnStructure {
    return {
      id: structure.columnId,
      filter: ColumnFactory.noFilter(structure.columnId),
      active: true,
      width: 100,
      displayWidth: 100,
      name: structure.label,
      center: true,
      sortable: false,
      type: ColumnType.ACTION,
      customContent: undefined,
      actions: structure.actions
    }
  }

  private static noFilter(id: string) {
    return {
      filterId: `${id}_no_filter`,
      operatorId: '',
      placeHolder: '',
      type: FilterType.NONE,
      operators: [],
      list: []
    }
  }

  private static resolveWidth(type: FilterType): number {
    let width = 100

    switch (type) {
      case FilterType.FLOAT:
      case FilterType.INTEGER:
        width = 140;
        break;
      case FilterType.TEXT:
        width = 160;
        break;
      case FilterType.DATE_RANG:
        width = 140;
        break;
      case FilterType.SELECT:
      case FilterType.ASYNC_SELECT:
        width = 100;
        break;
    }

    return width;
  }

  private static resolveOperator(type: FilterType): Operator[] {
    switch (type) {
      case FilterType.DATE_RANG:
        return [Operator.IN_RANG]
      case FilterType.TEXT:
        return [
          Operator.LIKE,
          Operator.EQUAL,
          Operator.NOT_EQUAL,
        ]
      case FilterType.NONE:
        return []
      case FilterType.SELECT:
      case FilterType.ASYNC_SELECT:
        return [Operator.IN]
      case FilterType.INTEGER:
      case FilterType.FLOAT:
        return [
          Operator.EQUAL,
          Operator.NOT_EQUAL,
          Operator.GREATER_THEN,
          Operator.LESS_THEN,
          Operator.GREATER_THEN_OR_EQUAL,
          Operator.LESS_THEN_OR_EQUAL
        ]
    }
  }
}
