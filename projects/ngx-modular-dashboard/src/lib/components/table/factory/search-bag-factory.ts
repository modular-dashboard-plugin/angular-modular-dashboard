import {FilterBox, SearchBag, SortBox} from "../model/search-bag";
import {FilterType} from "../model/enums/filter-type";
import {Operator} from "../model/enums/operator";
import {SearchFilterType} from "../model/enums/search-filter-type";
import {isMoment} from "moment";
import {TableComponent} from "../table.component";


export class SearchBagFactory {

  static extractSearchBag(component: TableComponent): SearchBag {

    const filters: FilterBox[] = []

    component.structure.columns.forEach(column => {
      let input = component.form.controls[column.filter.filterId]
      let operator = component.form.controls[column.filter.operatorId]
      let filterName = column.filter.filterId.replace("_filter", "")

      if (input != null && input.value) {
        switch (column.filter.type) {
          case FilterType.SELECT:
          case FilterType.ASYNC_SELECT:
            if ((input.value as string[]).length !== 0)
              filters.push({
                id: filterName,
                value: input.value.join('~'),
                operator: Operator.IN,
                type: SearchFilterType.ARRAY
              })
            break
          case FilterType.TEXT:
            if (operator != null && operator.value != null) {
              filters.push({
                id: filterName,
                value: input.value,
                operator: operator.value,
                type: SearchFilterType.STRING
              })
            }
            break
          case FilterType.INTEGER:
            if (operator != null && operator.value != null) {
              filters.push({
                id: filterName,
                value: input.value.toString(),
                operator: operator.value,
                type: SearchFilterType.INT
              })
            }
            break
          case FilterType.FLOAT:
            if (operator != null && operator.value != null) {
              filters.push({
                id: filterName,
                value: input.value.toString(),
                operator: operator.value,
                type: SearchFilterType.FLOAT
              })
            }
            break
          case FilterType.NONE:
            break
        }
      }
      if (column.filter?.type === FilterType.DATE_RANG) {
        let startValue = component.form.controls[`${column.filter.filterId}`]?.value
        let endValue = component.form.controls[`${column.filter.operatorId}`]?.value

        let dateFilterName = filterName.replace('_start', '')

        if (startValue !== null && endValue !== null && isMoment(startValue) && isMoment(endValue)) {
          let startDate = startValue.format("yyyy-MM-DD hh:mm:ss")
          let endDate = endValue.format("yyyy-MM-DD hh:mm:ss")

          if (startDate !== null && endDate !== null) {
            filters.push({
              id: dateFilterName,
              value: `${startDate}~${endDate}`,
              operator: Operator.IN_RANG,
              type: SearchFilterType.DATE
            })
          }
        }
      }
    })


    const sorts: SortBox[] = []

    component.table.sortParams.forEach((field: string, index: number) => {
      sorts.push({sortField: field, sortDirection: component.table.sortDirs[index]})
    })

    return {
      filters: filters,
      sorts: sorts,
      pageBox: {
        page: component.table.pageIndex ?? 0,
        pageSize: component.table.pageSize ?? 10
      }
    }
  }

}
