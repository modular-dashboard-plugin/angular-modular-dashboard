import {Component, Input} from "@angular/core";
import {TableColumnStructure} from "../../model/table-column-structure";
import {FilterListItem} from "../../model/filter-list-item";
import {FilterListItemType} from "../../model/enums/filter-list-item-type";


@Component({
  selector: 'ng-modular-list-column',
  styleUrls: ['../table.cell.scss'],
  template: `
    <div class="cell-box">
      <mat-chip-list [ngSwitch]="item.type">
          <span>
            <mat-chip *ngSwitchCase="'SUCCESS'" selected class="CHIP SUCCESS">{{item.name}}</mat-chip>
            <mat-chip *ngSwitchCase="'DANGER'" selected class="CHIP DANGER">{{item.name}}</mat-chip>
            <mat-chip *ngSwitchCase="'WARNING'" selected class="CHIP WARNING">{{item.name}}</mat-chip>
            <mat-chip *ngSwitchCase="'NORMAL'" selected class="CHIP NORMAL">{{item.name}}</mat-chip>
          </span>
      </mat-chip-list>
    </div>`
})
export class ListColumn {
  @Input() row!: any
  @Input() column!: TableColumnStructure

  get item(): FilterListItem {
    return this.column.filter?.list?.find(item => item.value == this.row[this.column.id]) ?? {
      value: this.row[this.column.id],
      name: this.row[this.column.id],
      type: FilterListItemType.NORMAL
    }
  }
}
