import {FilterListItemType} from "./enums/filter-list-item-type";

export interface FilterListItem {
  readonly value: string
  readonly name: string
  readonly type: FilterListItemType
}
