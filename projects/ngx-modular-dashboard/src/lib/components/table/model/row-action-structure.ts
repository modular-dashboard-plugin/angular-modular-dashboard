import {RowActionType} from "./enums/row-action-type";

export interface RowActionViewStructure<TYPE> {
  readonly name: (data: TYPE) => string | null
  readonly type: (data: TYPE) => RowActionType
  readonly display: (data: TYPE) => boolean
  readonly callback: (row: any) => void
}

export interface RowActionStructure<TYPE> {
  readonly name: string
  readonly type: RowActionType
  readonly display?: (data: TYPE) => boolean
  readonly callback: (row: any) => void
}


export interface DynamicRowActionStructure<TYPE> {
  readonly name: (data: TYPE) => string,
  readonly type: (data: TYPE) => RowActionType
  readonly display?: (data: TYPE) => boolean
  readonly callback: (row: any) => void
}
