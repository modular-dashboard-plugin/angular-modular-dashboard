import {FilterStructure} from "./filter-structure";
import {RowActionViewStructure} from "./row-action-structure";
import {ColumnType} from "./enums/column-type";
import {Type} from "@angular/core";
import {PlainColumnContent} from "../components/columns/plain-column.component";

export interface TableColumnStructure {
  readonly id: string
  readonly active: boolean
  readonly width: number
  displayWidth: number
  readonly name: string
  readonly center: boolean
  readonly sortable: boolean
  readonly filter: FilterStructure
  readonly type: ColumnType
  readonly customContent?: Type<PlainColumnContent>
  readonly customContentSelector?: (data: any) => string | number | boolean | {}
  readonly onClick?: (data: any) => void
  readonly actions: RowActionViewStructure<any>[]
}
