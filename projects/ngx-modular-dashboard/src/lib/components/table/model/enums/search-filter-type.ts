
export enum SearchFilterType {
  STRING = "string",
  INT = "int",
  FLOAT = "float",
  ARRAY = "array",
  DATE = "date",
}
