// noinspection JSUnusedGlobalSymbols
export enum FilterListItemType {
  NORMAL = "NORMAL",
  SUCCESS = "SUCCESS",
  DANGER = "DANGER",
  WARNING = "WARNING"
}
