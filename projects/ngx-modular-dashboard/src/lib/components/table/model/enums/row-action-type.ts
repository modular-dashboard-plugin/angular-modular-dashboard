// noinspection JSUnusedGlobalSymbols
export enum RowActionType {
  NORMAL = "NORMAL",
  WARNING = "WARNING"
}
