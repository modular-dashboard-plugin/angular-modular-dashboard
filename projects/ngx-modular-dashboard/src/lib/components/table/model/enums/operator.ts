
export enum Operator {
  EQUAL = "eq",
  NOT_EQUAL = "neq",
  LIKE = "lk",
  GREATER_THEN = "gt",
  LESS_THEN = "lt",
  GREATER_THEN_OR_EQUAL = "gte",
  LESS_THEN_OR_EQUAL = "lte",
  IN = "in",
  IN_RANG = "inr"
}
