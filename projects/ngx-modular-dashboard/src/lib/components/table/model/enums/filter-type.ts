
export enum FilterType {
  TEXT = "TEXT",
  SELECT = "SELECT",
  ASYNC_SELECT = "ASYNC_SELECT",
  INTEGER = "INTEGER",
  FLOAT = "FLOAT",
  DATE_RANG = "DATE_RANG",
  NONE = "NONE"
}
