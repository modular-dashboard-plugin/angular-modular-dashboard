export interface HeaderActionViewStructure {
  readonly name: string
  readonly display: () => boolean
  readonly callback: () => void
}

export interface HeaderActionStructure {
  readonly name: string
  readonly display?: () => boolean
  readonly callback: () => void
}
