import {Observable} from "rxjs";
import {TableResponse} from "./table-response";
import {TableColumnStructure} from "./table-column-structure";
import {HeaderActionViewStructure} from "./header-action-structure";
import {SearchBag} from "./search-bag";
import {TableStyle} from "./table-style";

export interface TableStructure {
  style: TableStyle
  reloadEvent?: Observable<void>
  defaultSortParams?: string[]
  defaultSortDirs?: string[]
  totalElements?: number
  headerActions?: HeaderActionViewStructure[]
  loadData: (searchBag: SearchBag) => Observable<TableResponse<any>>
  columns: TableColumnStructure[]
  hideHeader?: boolean
}
