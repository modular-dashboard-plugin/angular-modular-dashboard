export interface TableResponse<TYPE> {
  data: TYPE[]
  count: number
  page: number
  pageSize: number
}
