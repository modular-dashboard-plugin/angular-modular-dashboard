import {Art} from "../../common/art";

export interface TableStyle {
  loadingAnimation?: string


  emptyArt?: Art
  errorArt?: Art

  title: string
  headerColor?: string
  elevation: number
}
