export interface RowActionEvent<TYPE> {
  id: string
  row: TYPE
}
