import {Observable} from "rxjs";
import {RowActionViewStructure} from "./row-action-structure";
import {FilterType} from "./enums/filter-type";
import {FilterListItem} from "./filter-list-item";
import {Type} from "@angular/core";
import {PlainColumnContent} from "../components/columns/plain-column.component";

interface ColumnStructure {
  readonly columnId: string
  readonly label: string
  readonly center?: boolean
  readonly sortable?: boolean
  readonly hide?: boolean
  readonly customContent?: Type<PlainColumnContent>
  readonly customContentSelector?: (data: any) => string | number | boolean | {}
}

interface ColumnFilterStructure extends ColumnStructure {
  readonly filterId?: string
}

export interface ColumnTypeFilterStructure extends ColumnFilterStructure {
  readonly type: FilterType
  readonly click?: (data: any) => void
}

export interface ColumnSelectFilterStructure extends ColumnFilterStructure {
  readonly list: FilterListItem[],
  readonly noFilter: boolean
}

export interface ColumnAsyncSelectFilterStructure extends ColumnFilterStructure {
  readonly list: Observable<FilterListItem[]>,
  readonly noFilter: boolean
}

export interface ColumnActionStructure {
  readonly columnId: string
  readonly label: string
  readonly actions: RowActionViewStructure<any>[]
}

export interface ColumnNoFilterStructure extends ColumnStructure {
  readonly click?: (data: any) => void
}
