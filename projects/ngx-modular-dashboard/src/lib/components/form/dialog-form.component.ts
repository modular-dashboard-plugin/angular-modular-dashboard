import {Component, EventEmitter, Input, OnDestroy, Output, ViewChild} from '@angular/core';

import {Subject, Subscription} from "rxjs";
import {ApiError} from "../../infrastructure/api/api-error";
import {InternalFormComponent} from "./components/internal-form.component";
import {ValidationEvent} from "./model/enums/validation-event-type";
import {DialogFormStructure} from "./model/dialog-form-structure";
import {DialogFormEvent} from "./model/enums/dialog-form-events";
import {FormExtractErrors} from "./factory/extract-erros";

@Component({
  selector: 'ng-modular-dialog-form',
  styleUrls: ['./components/error-box.scss'],
  template: `
    <!--suppress JSUnusedGlobalSymbols -->
    <h1 mat-dialog-title>{{structure.title}}</h1>
    <div *ngIf="error!== null" class="error-box" fxFill fxLayout="row">
      <div class="icon-box" fxFlex="none">
        <mat-icon>sms_failed</mat-icon>
      </div>
      <div class="text-box" fxFlex="grow">
        {{error}}
      </div>
    </div>
    <mat-dialog-content fxFill>
      <ng-modular-form
        #internalFormComponent
        (dataEmitter)="onDataEmitted($event)"
        (stateEmitter)="onStateEmitted($event)"
        [inputs]="structure.inputs"
        [events]="actionSubject.asObservable()"
        [crossValidators]="structure.crossValidators ?? []"
        [formStyle]="structure.style">
      </ng-modular-form>
    </mat-dialog-content>
    <mat-dialog-actions fxFill fxLayoutAlign="end center">
      <button mat-button (click)="eventEmitter.next(DialogEvent.CANCELED)"
              [disabled]="loading">{{structure.cancelButton}}</button>
      <button mat-button (click)="actionSubject.next()" [disabled]="loading || invalid" color="primary">
        <ng-container *ngIf="loading else TEXT">
          <mat-spinner mode="indeterminate" diameter="20"></mat-spinner>
        </ng-container>
        <ng-template #TEXT>{{structure.submitButton}}</ng-template>
      </button>
    </mat-dialog-actions>
  `
})
export class DialogFormComponent implements OnDestroy {

  DialogEvent = DialogFormEvent

  @Input() structure !: DialogFormStructure
  @Output() eventEmitter: EventEmitter<DialogFormEvent> = new EventEmitter<DialogFormEvent>()
  @Output() apiErrorEmitter: EventEmitter<any> = new EventEmitter<any>()

  invalid: boolean = true
  loading: boolean = false
  error: string | null = null

  actionSubject: Subject<void> = new Subject<void>();

  private submitSubscription: Subscription | null = null

  constructor() {

  }

  @ViewChild('internalFormComponent') formComponent!: InternalFormComponent

  onStateEmitted(event: ValidationEvent): void {
    this.invalid = event != ValidationEvent.VALID
    if (event == ValidationEvent.INVALID_ERROR) {
      const errors = FormExtractErrors.extract({
        form: this.formComponent.form,
        crossValidators: this.structure.crossValidators ? this.structure.crossValidators : [],
        inputs: this.structure.inputs
      });

      if (errors.length > 0) {
        this.error = errors[0]
      }
    } else if (event == ValidationEvent.VALID) {
      this.error = null
    }
  }

  onDataEmitted(data: any): void {
    this.loading = true

    this.submitSubscription = this.structure.onDataSubmitted(data, this.formComponent.form).subscribe({
      next: _ => this.handleSuccess(),
      error: (error: ApiError) => this.handleError(error)
    })
  }

  private handleSuccess() {
    this.loading = false
    this.error = null
    this.eventEmitter.next(DialogFormEvent.SUBMITTED)
  }

  private handleError(error: ApiError) {
    this.loading = false
    this.error = error.errorMessage
    this.apiErrorEmitter.next(error)
  }

  ngOnDestroy(): void {
    this.submitSubscription?.unsubscribe()
  }

}
