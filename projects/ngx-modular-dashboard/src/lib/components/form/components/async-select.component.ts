import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {InputViewStructure} from "../model/input-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {InputListItem} from "../model/input-list-item";
import {ApiError} from "../../../infrastructure/api/api-error";
import {BaseInput} from "./base-input-component";
import {Subscription} from "rxjs";
import {FormStyle} from "../model/form-style";

@Component({
  selector: 'ng-modular-async-select-input',
  template: `
    <div fxFill>
      <ng-container *ngIf="formStyle.labelAppearance === LabelAppearance.OUTSIDE">
        <div>
          <mat-label style="color: VAR(--color-primary-darker)">{{input.label}}</mat-label>
        </div>
      </ng-container>
      <div style="width: 100%" fxLayout="row">
        <mat-form-field fxFill ngDefaultControl [id]="input.id" [appearance]="fieldAppearance" [floatLabel]="floatingLabel"
                        [formControl]="control">
          <mat-label *ngIf="formStyle.labelAppearance !== LabelAppearance.OUTSIDE">{{input.label}}</mat-label>
          <mat-select placeholder="{{input.longLabel}}" [formControl]="control">
            <mat-option *ngFor="let item of list" [value]="item.value">{{item.name}}</mat-option>
          </mat-select>
          <mat-spinner *ngIf="loading" matSuffix [diameter]="18" style="float: right; margin-left: 8px"></mat-spinner>
          <mat-icon *ngIf="showError" matSuffix (click)="reload()">refresh</mat-icon>
          <mat-error *ngIf="showError">{{errorMessage}}</mat-error>
        </mat-form-field>
      </div>
    </div>`
})
export class AsyncSelectInputComponent extends BaseInput implements OnInit, OnDestroy {
  @Input() input!: InputViewStructure
  @Input() control!: FormControl
  @Input() formStyle!: FormStyle
  @Input() formGroup !: FormGroup

  list: InputListItem[] = []
  loading = true

  private asyncListSubscription: Subscription | null = null

  error = false

  ngOnInit(): void {
    this.loadList();
  }

  private loadList() {
    if (this.input.asyncList !== undefined)
      this.asyncListSubscription = this.input.asyncList.subscribe({
        next: list => {
          this.list = list
          this.error = false
          this.loading = false
        },
        error: (error: ApiError) => {
          this.error = true
          this.loading = false
          console.log(error)
        }
      })
  }

  ngOnDestroy(): void {
    this.asyncListSubscription?.unsubscribe()
  }


  reload() {
    this.error = false
    this.loading = true
    this.loadList()
  }

  get showError() {
    return (this.hasError && (this.control.dirty || this.control.touched)) || this.error
  }

  get errorMessage() {
    if (this.hasError && (this.control.dirty || this.control.touched))
      return this.message
    else
      return 'Failed. Try Again.'
  }
}
