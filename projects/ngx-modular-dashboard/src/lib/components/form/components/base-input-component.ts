import {InputViewStructure} from "../model/input-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {FieldAppearance} from "../model/enums/field-appearance";
import {LabelAppearance} from "../model/enums/label-appearance";
import {FormStyle} from "../model/form-style";

export abstract class BaseInput {

  LabelAppearance = LabelAppearance

  abstract input: InputViewStructure
  abstract control: FormControl
  abstract formStyle: FormStyle
  abstract formGroup: FormGroup
  defaultErrorMessage: string = '';


  get message(): string {
    for (let validator of this.input.validators) {
      if (this.control.hasError(validator.id)) {
        return validator.message
      }
      if (this.formGroup.hasError(validator.id)) {
        return validator.message
      }
    }
    return this.defaultErrorMessage
  }

  get hasError() {
    for (let validator of this.input.validators) {
      if (this.control.hasError(validator.id)) {
        return true
      }
      if (this.formGroup.hasError(validator.id)) {
        return true
      }
    }
    return false
  }


  get floatingLabel() {
    if (this.formStyle.labelAppearance === LabelAppearance.FLOATING)
      return 'always'
    else
      return 'never'
  }

  get fieldAppearance() {
    switch (this.formStyle.fieldAppearance) {
      case FieldAppearance.LEGACY:
        return 'legacy';
      case FieldAppearance.STANDARD:
        return 'standard';
      case FieldAppearance.OUTLINE:
        return 'outline';
      case FieldAppearance.FILL:
        return 'fill';
    }
  }

}
