import {Component, Input, OnInit} from "@angular/core";
import {InputViewStructure} from "../model/input-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {BaseInput} from "./base-input-component";
import {FormStyle} from "../model/form-style";

@Component({
  selector: 'ng-modular-new-password-input',
  template: `
    <div fxFill>
      <ng-container *ngIf="formStyle.labelAppearance === LabelAppearance.OUTSIDE">
        <div>
          <mat-label style="color: VAR(--color-primary-darker)">{{input.label}}</mat-label>
        </div>
      </ng-container>
      <div style="width: 100%" fxLayout="row">
        <mat-form-field fxFill ngDefaultControl [id]="input.id" [appearance]="fieldAppearance"
                        [floatLabel]="floatingLabel"
                        [formControl]="control">
          <mat-label *ngIf="formStyle.labelAppearance !== LabelAppearance.OUTSIDE">{{input.label}}</mat-label>
          <input matInput placeholder="{{input.longLabel}}" [type]="hide ? 'password' : 'text'" [formControl]="control">
          <mat-icon matSuffix (click)="hide = !hide">{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>
          <mat-error *ngIf="hasError && (control.dirty || control.touched)">{{message}}</mat-error>
        </mat-form-field>
      </div>
      <ng-container *ngIf="formStyle.labelAppearance === LabelAppearance.OUTSIDE">
        <div>
          <mat-label style="color: VAR(--color-primary-darker)">Confirm {{input.label}}</mat-label>
        </div>
      </ng-container>
      <div style="width: 100%" fxLayout="row">
        <mat-form-field fxFill ngDefaultControl id="confirm_{{input.id}}" [appearance]="fieldAppearance"
                        [floatLabel]="floatingLabel" [formControl]="confirmControl">
          <mat-label *ngIf="formStyle.labelAppearance !== LabelAppearance.OUTSIDE">Confirm {{input.label}}</mat-label>
          <input matInput placeholder="Confirm {{input.longLabel}}" [type]="hide ? 'password' : 'text'"
                 [formControl]="confirmControl">
          <mat-icon matSuffix (click)="hide = !hide">{{hide ? 'visibility_off' : 'visibility'}}</mat-icon>
          <mat-error *ngIf="hasErrorForConfirm && (control.dirty || control.touched)">{{messageForConfirm}}</mat-error>
        </mat-form-field>
      </div>
    </div>`
})
export class NewPasswordComponent extends BaseInput implements OnInit {
  @Input() input!: InputViewStructure
  @Input() control!: FormControl
  @Input() formStyle!: FormStyle
  @Input() confirmControl!: FormControl
  @Input() formGroup !: FormGroup


  hide = true


  ngOnInit(): void {

  }


  get messageForConfirm(): string {
    for (let validator of this.input.validators) {
      if (this.confirmControl.hasError(validator.id)) {
        return validator.message
      }
      if (this.formGroup.hasError(validator.id)) {
        return validator.message
      }
    }
    return this.defaultErrorMessage
  }

  get hasErrorForConfirm() {
    for (let validator of this.input.validators) {
      if (this.confirmControl.hasError(validator.id)) {
        return true
      }
      if (this.formGroup.hasError(validator.id)) {
        return true
      }
    }
    return false
  }

}
