import {Component, Input} from "@angular/core";
import {InputViewStructure} from "../model/input-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {BaseInput} from "./base-input-component";
import {FormStyle} from "../model/form-style";

@Component({
  selector: 'ng-modular-area-input',
  template: `
    <div fxFill>
      <ng-container *ngIf="formStyle.labelAppearance === LabelAppearance.OUTSIDE">
        <div>
          <mat-label style="color: VAR(--color-primary-darker)">{{input.label}}</mat-label>
        </div>
      </ng-container>
      <div style="width: 100%" fxLayout="row">
        <mat-form-field fxFill ngDefaultControl [id]="input.id"  [appearance]="fieldAppearance" [floatLabel]="floatingLabel" [formControl]="control">
          <mat-label *ngIf="formStyle.labelAppearance !== LabelAppearance.OUTSIDE">{{input.label}}</mat-label>
          <textarea matInput placeholder="{{input.longLabel}}" type="text" [formControl]="control"></textarea>
          <mat-error *ngIf="hasError && (control.dirty || control.touched)">{{message}}</mat-error>
        </mat-form-field>
      </div>
    </div>`
})
export class TextareaInputComponent extends BaseInput {
  @Input() input!: InputViewStructure
  @Input() control!: FormControl
  @Input() formStyle!: FormStyle
  @Input() formGroup !: FormGroup
}
