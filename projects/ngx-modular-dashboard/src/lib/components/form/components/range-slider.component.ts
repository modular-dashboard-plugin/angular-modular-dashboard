import {Component, Input} from "@angular/core";
import {InputViewStructure} from "../model/input-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {BaseInput} from "./base-input-component";
import {FormStyle} from "../model/form-style";
import {Options} from "@angular-slider/ngx-slider";
import {LabelType} from "@angular-slider/ngx-slider/options";

@Component({
  selector: 'ng-modular-range-slider-input',
  template: `
    <div fxFill>
      <div style="width: 100%; min-height: 54px" fxLayout="column" class="slider-container">
        <mat-label style="color: VAR(--color-primary-darker)">{{input.label}}</mat-label>
        <ngx-slider [options]="options" [formControl]="control" [(value)]="input.minLimit!"
                    [(highValue)]="input.maxLimit!"></ngx-slider>
        <div style="font-size: 12px!important;padding-bottom: 32px">
          <mat-error *ngIf="hasError && control.touched">{{message}}</mat-error>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['range-slider.component.scss']
})
export class RangeSliderComponent extends BaseInput {
  @Input() input!: InputViewStructure
  @Input() control!: FormControl
  @Input() formStyle!: FormStyle
  @Input() formGroup !: FormGroup


  get options(): Options {
    return {
      minLimit: this.input.minLimit,
      maxLimit: this.input.maxLimit,
      floor: 0,
      ceil: this.input.maxLimit,
      step: this.input.maxLimit! / 20,
      tickStep: this.input.maxLimit! / 20,
      translate: (value: number, _: LabelType): string => {
        if (value >= 1000000) {
          return Math.round(value / 1000000) + 'M';
        }
        if (value >= 1000) {
          return Math.round(value / 1000) + 'K';
        }

        return value.toString();
      },
    };
  }

}
