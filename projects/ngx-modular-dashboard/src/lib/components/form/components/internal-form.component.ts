import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core'
import {
  AbstractControlOptions,
  AsyncValidatorFn,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators
} from "@angular/forms"
import {Observable, Subscription} from "rxjs"
import {InputViewStructure} from "../model/input-structure"
import {ValidationEvent} from "../model/enums/validation-event-type"
import {InputType} from "../model/enums/input-type"
import {FormStyle} from "../model/form-style";
import {InputValidator} from "../model/input-validator";
import {Dimensions} from "../../common/dimensions";
import {FlexDefinition} from "../../common/flex-definition";
import {FlexFactory} from "../../common/flex-factory";


@Component({
  selector: 'ng-modular-form',
  template: `
    <form fxFill fxLayout="row wrap" [formGroup]="form">
      <ng-container *ngFor="let input of inputs">
        <ng-container *ngIf="input.display(form.value)" [ngSwitch]="inputType(input)">

          <ng-container *ngSwitchCase="'TITLE'">
            <div class="input-box section-title" fxFlex="100">{{input.value}}</div>
          </ng-container>

          <ng-container *ngSwitchCase="'DATE'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-date-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-date-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-date-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-date-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'SELECT'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-select-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-select-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-select-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-select-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'ASYNC_SELECT'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-async-select-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-async-select-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-async-select-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-async-select-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'AUTO_COMPLETE'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-auto-complete-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-auto-complete-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-auto-complete-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-auto-complete-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'AREA'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-area-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-area-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-area-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-area-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'TYPE'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-type-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-type-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-type-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-type-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'PASSWORD'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-password-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-password-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-password-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-password-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'NEW_PASSWORD'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-new-password-input
                class="input-box"
                [control]="control(input)"
                [confirmControl]="confirmPasswordControl(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-new-password-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-new-password-input
                class="input-box"
                [control]="control(input)"
                [confirmControl]="confirmPasswordControl(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-new-password-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'FILE'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-file-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-file-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-file-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-file-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'CHECK_BOX'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-check-box-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-check-box-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-check-box-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-check-box-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'SLIDER'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-slider-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-slider-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-slider-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-slider-input>
            </ng-template>
          </ng-container>

          <ng-container *ngSwitchCase="'RANGE_SLIDER'">
            <ng-container *ngIf="dimensions(input.dimensions).width !== undefined else FLEX">
              <ng-modular-range-slider-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.lt-md="100"
                [ngStyle]="{'width.px': dimensions(input.dimensions).width}"
                [ngStyle.lt-md]="{'width.px': null}">
              </ng-modular-range-slider-input>
            </ng-container>

            <ng-template #FLEX>
              <ng-modular-range-slider-input
                class="input-box"
                [control]="control(input)"
                [input]="input"
                [formGroup]="form"
                [formStyle]="formStyle"
                fxFlex.gt-md="1 1 {{flex(input.dimensions).lg}}%"
                fxFlex.md="1 1 {{flex(input.dimensions).md}}%"
                fxFlex.sm="1 1 {{flex(input.dimensions).sm}}%"
                fxFlex.xs="1 1 {{flex(input.dimensions).xs}}%">
              </ng-modular-range-slider-input>
            </ng-template>
          </ng-container>

        </ng-container>
      </ng-container>
    </form>
  `,
  styles: [
    `.input-box {
      padding-left: 8px;
      padding-right: 8px;
    }

    .section-title {
      text-align: left;
      font-size: 20px;
      font-weight: bold;
      padding: 4px;
      margin-bottom: 16px;
      margin-top: 32px;
    }
    `
  ]
})
export class InternalFormComponent implements OnInit, OnDestroy {

  loading: boolean = false
  form!: FormGroup

  private eventsSubscription: Subscription | null = null
  private formStateSubscription: Subscription | null = null
  private enableStateSubscription: Subscription | null = null

  @Input() events!: Observable<void>
  @Input() inputs!: InputViewStructure[]
  @Input() formStyle!: FormStyle
  @Input() crossValidators!: InputValidator[]

  @Output() dataEmitter: EventEmitter<any> = new EventEmitter<any>()
  @Output() stateEmitter: EventEmitter<ValidationEvent> = new EventEmitter<ValidationEvent>()

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnDestroy(): void {
    this.eventsSubscription?.unsubscribe()
    this.formStateSubscription?.unsubscribe()
    this.enableStateSubscription?.unsubscribe()
  }

  ngOnInit(): void {
    this.initializeForm()
    this.eventsSubscription = this.events.subscribe(() => this.onSubmit())
  }


  private initializeForm() {

    const options: AbstractControlOptions = {
      updateOn: 'change',
      validators: Validators.compose(InternalFormComponent.getValidators(this.crossValidators)),
      asyncValidators: Validators.composeAsync(InternalFormComponent.getAsyncValidators(this.crossValidators)),
    }
    let group = this.formBuilder.group({}, options)


    this.inputs.forEach(input => {
      const options: AbstractControlOptions = {
        validators: Validators.compose(InternalFormComponent.getValidators(input.validators)),
        asyncValidators: Validators.composeAsync(InternalFormComponent.getAsyncValidators(input.validators)),
      }

      if (input.type == InputType.TITLE) {
        return
      }


      group.addControl(input.id, new FormControl(input.value, options))

      if (input.type === InputType.NEW_PASSWORD) {
        group.addControl(`confirm_${input.id}`, new FormControl(input.value, options))
      }

    })

    this.form = group

    this.formStateSubscription = this.form.statusChanges.subscribe(_ => {
      this.stateEmitter.emit(this.form.invalid ? ValidationEvent.INVALID : ValidationEvent.VALID)
    })

  }


  private static getValidators(inputValidators: InputValidator[]): ValidatorFn[] {
    const validators: ValidatorFn[] = []

    inputValidators.forEach(validator => {
      if (validator.validatorFun !== undefined) {
        validators.push(validator.validatorFun)
      }
    })
    return validators
  }


  private static getAsyncValidators(inputValidators: InputValidator[]): AsyncValidatorFn[] {
    const validators: AsyncValidatorFn[] = []

    inputValidators.forEach(validator => {
      if (validator.asyncValidatorFun !== undefined) {
        validators.push(validator.asyncValidatorFun)
      }
    })
    return validators
  }

  control(input: InputViewStructure): FormControl {
    return this.form.get(input.id) as FormControl
  }

  confirmPasswordControl(input: InputViewStructure): FormControl {
    return this.form.get(`confirm_${input.id}`) as FormControl
  }

  inputType(input: InputViewStructure): string {
    switch (input.type) {
      case InputType.ASYNC_SELECT:
        return 'ASYNC_SELECT'
      case InputType.AUTO_COMPLETE:
        return 'AUTO_COMPLETE'
      case InputType.DATE:
        return 'DATE'
      case InputType.SELECT:
        return 'SELECT'
      case InputType.TEXT_AREA:
        return 'AREA'
      case InputType.FILE:
        return 'FILE'
      case InputType.EMAIL:
      case InputType.NUMBER:
      case InputType.PHONE:
      case InputType.TEXT:
        return 'TYPE'
      case InputType.PASSWORD:
        return 'PASSWORD'
      case InputType.NEW_PASSWORD:
        return 'NEW_PASSWORD'
      case InputType.CHECK_BOX:
        return 'CHECK_BOX'
      case InputType.SLIDER:
        return 'SLIDER'
      case InputType.RANGE_SLIDER:
        return 'RANGE_SLIDER'
      case InputType.TITLE:
        return 'TITLE'
    }
  }


  private onSubmit(): void {
    if (this.form.invalid) {
      console.log(this.form)
      this.form.markAllAsTouched()
      this.stateEmitter.emit(this.form.invalid ? ValidationEvent.INVALID_ERROR : ValidationEvent.VALID)
      return
    }


    const objWithoutNull = this.form.value;
    Object.keys(objWithoutNull).forEach(key => {
      if (objWithoutNull[key] === '') {
        objWithoutNull[key] = null;
      } else if (this.inputs.find(input => input.id === key)?.type === InputType.RANGE_SLIDER) {
        objWithoutNull[key] = (<number[]>objWithoutNull[key]).join(",").toString();
      }
    });

    this.dataEmitter.emit(objWithoutNull)
  }


  dimensions(dimensions: Dimensions) {
    return this.formStyle.dimensions !== undefined ? this.formStyle.dimensions : dimensions
  }


  flex(dimensions: Dimensions): FlexDefinition {
    if (dimensions.flex !== undefined) {
      return dimensions.flex
    } else if (this.formStyle.dimensions !== undefined && this.formStyle.dimensions.flex !== undefined) {
      return this.formStyle.dimensions.flex
    } else {
      return FlexFactory.defaultFlex().flex!
    }
  }
}
