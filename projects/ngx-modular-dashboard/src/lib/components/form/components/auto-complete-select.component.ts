import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {InputViewStructure} from "../model/input-structure";
import {FormControl, FormGroup} from "@angular/forms";
import {SearchResult} from "../model/search-result";
import {BaseInput} from "./base-input-component";
import {Subscription} from "rxjs";
import {FormStyle} from "../model/form-style";

@Component({
  selector: 'ng-modular-auto-complete-input',
  template: `
    <div fxFill>
      <ng-container *ngIf="formStyle.labelAppearance === LabelAppearance.OUTSIDE">
        <div>
          <mat-label style="color: VAR(--color-primary-darker)">{{input.label}}</mat-label>
        </div>
      </ng-container>
      <div style="width: 100%" fxLayout="row">
        <mat-form-field fxFill ngDefaultControl [id]="input.id"  [appearance]="fieldAppearance" [floatLabel]="floatingLabel" [formControl]="control">
          <mat-label *ngIf="formStyle.labelAppearance !== LabelAppearance.OUTSIDE">{{input.label}}</mat-label>
          <mat-select placeholder="{{input.longLabel}}" [formControl]="control">
            <mat-form-field appearance="outline" fxFill style="margin: 16px!important">
              <input autocomplete="doNotAutoComplete" placeholder="Search" matInput (keyup)="onSearchEvent($event)">
              <mat-spinner
                *ngIf="loading" matSuffix [diameter]="18" style="float: right; margin-left: 8px"></mat-spinner>
            </mat-form-field>
            <mat-option>None</mat-option>
            <mat-option *ngFor="let obj of searchObjects" [value]="obj.id">{{obj.value}}</mat-option>
          </mat-select>
        </mat-form-field>
      </div>
    </div>
  `
})
export class AutocompleteInputComponent extends BaseInput implements OnInit, OnDestroy {
  @Input() input!: InputViewStructure
  @Input() control!: FormControl
  @Input() formStyle!: FormStyle
  @Input() formGroup !: FormGroup


  searchObjects: SearchResult[] = []

  loading = true
  error = false//TODO implement error view and retry action

  private autoCompleteSubscription: Subscription | null = null

  onSearchEvent(event: KeyboardEvent) {
    this.loading = true
    let value = (event.target as HTMLInputElement)?.value
    this.loadList(value)
  }

  ngOnInit(): void {
    this.loadList(null);
  }

  private loadList(value: string | null) {
    if (this.input.autoComplete !== undefined)
      this.autoCompleteSubscription = this.input.autoComplete(value).subscribe({
        next: response => {
          this.searchObjects = response
          this.error = false
          this.loading = false
        },
        error: error => {
          this.error = true
          this.loading = false
          console.log(error)
        }
      })
  }

  ngOnDestroy(): void {
    this.autoCompleteSubscription?.unsubscribe()
  }

  get showError() {
    return (this.hasError && (this.control.dirty || this.control.touched)) || this.error
  }

  get errorMessage() {
    if (this.hasError && (this.control.dirty || this.control.touched))
      return this.message
    else
      return 'Failed. Try Again.'
  }
}
