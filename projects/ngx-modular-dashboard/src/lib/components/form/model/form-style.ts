import {FieldAppearance} from "./enums/field-appearance";
import {LabelAppearance} from "./enums/label-appearance";
import {Dimensions} from "../../common/dimensions";

export interface FormStyle {
  fieldAppearance: FieldAppearance
  labelAppearance: LabelAppearance
  dimensions?: Dimensions
}
