export enum FieldAppearance {
  LEGACY = "legacy",
  STANDARD = "standard",
  FILL = "fill",
  OUTLINE = "outline",
}
