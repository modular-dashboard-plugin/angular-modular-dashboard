export enum LabelAppearance {
  FLOATING = "floating",
  OUTSIDE = "outside",
  INSIDE = "inside"
}
