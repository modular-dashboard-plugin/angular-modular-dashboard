import {Observable} from "rxjs";
import {Moment} from "moment";
import {InputValidator} from "./input-validator";
import {InputType} from "./enums/input-type";
import {SearchResult} from "./search-result";
import {InputListItem} from "./input-list-item";
import {BaseFileServer} from "../../../infrastructure/file-server/base-file-server";
import {Dimensions} from "../../common/dimensions";

export interface FormInputStructure {
  id: string
  label: string
  longLabel?: string
  value?: string | number | boolean | null
  display?: (formData: any) => boolean
  required?: boolean
  dimensions?: Dimensions
}

export interface TypeInputStructure extends FormInputStructure {
  type: InputType
  validators?: InputValidator[]
}

export interface SearchInputStructure extends FormInputStructure {
  autoComplete: (query?: string | null) => Observable<SearchResult[]>
}

export interface SelectInputStructure extends FormInputStructure {
  list: InputListItem[]
  multiSelect?: boolean
}

export interface AsyncSelectInputStructure extends FormInputStructure {
  asyncList: Observable<InputListItem[]>
}

export interface DateInputStructure extends FormInputStructure {
  maxDate?: Moment | null
  minDate?: Moment | null
}

export interface FileInputStructure<T extends BaseFileServer> extends FormInputStructure {
  fileServer: T
  fileType: string
}


export interface PasswordInputStructure extends FormInputStructure {
  forgetPassword?: () => void
  showForgetPassword?: boolean
  validators?: InputValidator[]
}


export interface NewPasswordInputStructure extends FormInputStructure {
  validators?: InputValidator[]
}

export interface CheckBoxInputStructure extends FormInputStructure {

}


export interface SlideInputStructure extends FormInputStructure {
  minLimit: number
  maxLimit: number
}

export interface RangeSlideInputStructure extends FormInputStructure {
  minLimit: number
  maxLimit: number
}
