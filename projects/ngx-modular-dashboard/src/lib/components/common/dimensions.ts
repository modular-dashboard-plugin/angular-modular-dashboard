import {FlexDefinition} from "./flex-definition";

export interface Dimensions {
  width?: number
  flex?: FlexDefinition
}

