#!/bin/bash

VERSION=$(cat ./projects/ngx-modular-dashboard/package.json | json version)


NEXT_VERSION=$(echo ${VERSION} | awk -F. -v OFS=. '{$NF++;print}')

json -I -f ./projects/ngx-modular-dashboard/package.json -e "this.version='${NEXT_VERSION}'"

cd projects/ngx-modular-dashboard

npm install

cd ../../

ng build ngx-modular-dashboard

cd dist/ngx-modular-dashboard

npm init -y

npm publish --access public

